=== Camptix Invoices ===
Contributors: willybahuaud, simonjanin
Tags: camptix, invoice, event, organizer, tickets, notes
Requires at least: 3.0.1
Tested up to: 4.9.2
Requires PHP: 5.4
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Text Domain: invoices-camptix
 
Allow Camptix administrators to send invoices automatically when a attendee buy a ticket.
 
== Description ==
 
Allow Camptix administrators to send invoices automatically when a attendee buy a ticket.

 
== Installation ==
 
1. Install and configure Camptix plugin
1. Download and activate Camptix Invoice through the 'Plugins' menu in WordPress
1. Go to Camptix settings to define Event information (organiser name, logo, invoice name)
 

== Frequently Asked Questions ==

= Can I add a custom invoice? =

Yes, you can create your own invoice (using the Camptix > Invoices submenu ).
Be warned that you can't edit or delete a published invoice, so… save it as draft before every items/information are ok!
 
= How can I customize Invoices template? =

You can drop a copy of file `gabarit.php` into your theme folder and name it `gabarit-invoice.php`.
Use CSS properties in this file to overide Invoice template.


== Changelog ==
 
= 1.0.0 =
* Initial release!
